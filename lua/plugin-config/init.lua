local path = 'plugin-config/'

require(path..'galaxyline-config')
require(path..'indent-blankline-config')
require(path..'kommentary-config')
require(path..'nvim-cmp-config')
require(path..'nvim-tree-config')
require(path..'nvim-treesitter-config')
require(path..'rnvimr-config')
require(path..'telescope-config')
require(path..'tokyonight-config')
