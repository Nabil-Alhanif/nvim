-- Make Ranger replace Netrw and be the default file explorer
vim.g.rnvimr_enable_ex = 1

-- Make ranger hidden after picking a file
vim.g.rnvimr_enable_picker = 1

-- Make neovim wipe the buffer corresponding to the files deleted by ranger
vim.g.rnvimr_enable_bw = 1
