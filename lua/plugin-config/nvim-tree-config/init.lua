require('nvim-tree').setup {
    require('plugin-config/nvim-tree-config/mappings'), -- Mappings for Nvim-Tree
    require('plugin-config/nvim-tree-config/settings') -- Settings for Nvim-Tree
}
