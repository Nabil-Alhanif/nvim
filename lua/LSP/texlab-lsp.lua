return {
    --cmd = { vim.fn.stdpath('data')..'/lspinstall/latex/texlab' },
    filetypes = {
        'tex',
        'bib',
        'plaintex',
    },
}
